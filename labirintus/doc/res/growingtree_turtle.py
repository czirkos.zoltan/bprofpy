import random
import turtle
import time

MERETX = 35
MERETY = 35
PIXEL = 16      # egy cella mérete
SLEEP = 40      # késleltetés

FAL = (1, 1, 1)
JARAT = (0, 0, 0)
IDEIGLENES = (0, 1, 0)

class Pont:
    def __init__(self, x = 0, y = 0):
        self.x = x
        self.y = y


# rajzolás: beleír a tömbbe, és adott színű négyzetet rajzol
def rajzol(kep, x, y, mit):    
    kep[y][x] = mit
    turtle.color(mit, mit)
    turtle.up()
    turtle.goto(x * PIXEL, y * PIXEL)
    turtle.setheading(0)
    turtle.down()
    turtle.begin_fill()
    for _ in range(4):
        turtle.forward(PIXEL)
        turtle.left(90)
    turtle.end_fill()
    turtle.color((1, 0, 0))


# üres képet csinál
def ures_kep():
    # Üres 2D tömb
    kep = []
    for _ in range(MERETY):
        kep.append([None]*MERETX)

    # keret, mintha ott járat lenne, és akkor nem megy oda a rajzoló
    for y in range(MERETY):
        rajzol(kep, 0, y, JARAT)
        rajzol(kep, MERETX - 1, y, JARAT)

    for x in range(MERETX):
        rajzol(kep, x, 0, JARAT)
        rajzol(kep, x, MERETY - 1, JARAT)

    # többi telibe fallal
    for y in range(1, MERETY - 1):
        for x in range(1, MERETX - 1):
            rajzol(kep, x, y, FAL)

    return kep


# A labirintus generáló
def labirintus_rajzol(kep, kiindul_x, kiindul_y):
    pontok = []
    # iránykoordináták (delta x, delta y)
    # [balra, jobbra, fel, le]
    iranyok = [Pont(-1, 0), Pont(1, 0), Pont(0, 1), Pont(0, -1)]

    # a kiindulási pontot kivájjuk
    kep[kiindul_y][kiindul_x] = JARAT
    pontok.append(Pont(kiindul_x, kiindul_y))

    while 0 < len(pontok):
        # kiveszünk egy elemet a listából, ezt fogjuk vizsgálni.
        # itt lehet állítani, hogy első (0), utolsó (len(pontok) - 1)
        # vagy másmelyik element vegye ki a listából.
        pont = random.choice(pontok)

        # választunk egy random irányt is
        irany = random.choice(iranyok)
        cel = Pont(pont.x + irany.x * 2, pont.y + irany.y * 2)

        # megpróbálunk menni arra
        if kep[cel.y][cel.x] == FAL:
            rajzol(kep, cel.x, cel.y, IDEIGLENES)                    # új terem
            rajzol(kep, pont.x + irany.x, pont.y + irany.y, JARAT)   # odavezető út
            pontok.append(cel)
            turtle.update()
            time.sleep(SLEEP/1000)

        # itt szebb lenne, ha összegyűjtenénk a lehetséges irányokat,
        # és utána abból választanánk véletlenszerűen, mert lehet, hogy
        # lehet menni valamerre, de a random pont nem azt az irányt
        # találja ki... összesen csak 4 irány van, úgyhogy ez a pongyolaság
        # most belefér

        # és akkor most, lehet még innen valahova menni?
        lehet_meg_valahova = False
        for i in iranyok:
            cel = Pont(pont.x + i.x * 2, pont.y + i.y * 2)
            if kep[cel.y][cel.x] == FAL:
                lehet_meg_valahova = True
                break

        # ha már nem, akkor kiszedjük a tárolóból
        if not lehet_meg_valahova:
            rajzol(kep, pont.x, pont.y, JARAT)
            pontok.remove(pont)
            turtle.update()
            time.sleep(SLEEP/1000)


def main():
    turtle.setup(MERETX * PIXEL, MERETY * PIXEL)
    turtle.setworldcoordinates(0, 0, MERETX * PIXEL, MERETY * PIXEL)
    turtle.tracer(False)

    kep = ures_kep()
    labirintus_rajzol(kep, MERETX // 2 + 1, MERETY // 2 + 1)

    time.sleep(5)


main()
