#include <stdio.h>
#include <stdbool.h>
 
#define MERETX 19
#define MERETY 13
 
typedef enum Cella {
    Jarat = ' ',
    Fal = 'X',
    Kijarat = '*',
    Zsakutca = '.',
    Kerdeses = '?'
} Cella;
 
/* a mainben levo sztring miatt ez most char tomb,
 * de a celjaink szempontjabol teljesen mindegy, hiszen
 * a fenti felsorolt ertekek szandekosan karakterkodok */
typedef char Palya[MERETY][MERETX + 1];
 
void kirajzol(Palya p) {
    int x, y;
 
    for (y = 0; y < MERETY; ++y) {
        for (x = 0; x < MERETX; ++x) {
            /* 2x irom ki, igy dupla szeles lesz - jobban lathato */
            putchar(p[y][x]);
            putchar(p[y][x]);
        }
        putchar('\n');
    }
}
 
/* igazzal ter vissza, ha arrafele megtalalta,
 * hamissal, ha nem */
bool megfejt(Palya p, int x, int y, int celx, int cely) {
    /* elso korben bejeloljuk ezt a helyet Kerdesesnek -
     * ez vegulis csak azert lenyeges, hogy ne jarat legyen
     * itt, mert akkor visszajohetne ide */
    p[y][x] = Kerdeses;
 
    /* egyelore nem talaljuk a Kijaratot */
    bool megtalalt = false;
 
    /* pont a cel8nal allunk? */
    if (x == celx && y == cely)
        megtalalt = true;
 
    /* ha meg nem talaltuk meg a Kijaratot... ES ha tudunk jobbra menni... */
    if (!megtalalt && x < MERETX - 1 && p[y][x + 1] == Jarat) {
        /* ha arra van a megfejtes */
        if(megfejt(p, x + 1, y, celx, cely))
            megtalalt = true;
    }
    /* balra */
    if (!megtalalt && x > 0 && p[y][x - 1] == Jarat) {
        if (megfejt(p, x - 1, y, celx, cely))
            megtalalt = true;
    }
    if (!megtalalt && y > 0 && p[y - 1][x] == Jarat) {
        if (megfejt(p, x, y - 1, celx, cely))
            megtalalt = true;
    }
    if (!megtalalt && y < MERETY - 1 && p[y + 1][x] == Jarat) {
        if (megfejt(p, x, y + 1, celx, cely))
            megtalalt = true;
    }
 
    /* viszont ha innen kiindulva meg lehet talalni a Kijaratot
     * (most mar tudjuk a fuggvenyek visszateresi ertekebol),
     * akkor a Kijarathoz vezeto utkent jeloljuk meg. */
    p[y][x] = megtalalt ? Kijarat : Zsakutca;
 
    /* jelezzuk a hivonak, hogy valahonnan errefele indulva
     * lehet-e Kijaratot talalni */
    return megtalalt;
}
 
int main(void) {
    Palya p = {
        /* itt sztringgekkel adom meg a palyat, hogy jol latsszon a
         * forraskodban a bemenet. azoknak van lezaro nullajuk.
         * ezert egy karakterrel szelesebbre vettem a tombot a tipus
         * megadasanal (typedef fent), de nem hasznalom a programban
         * a fordito altal odarakott lezaro nullat semmire. */
        "XXXXXXXXXXXXXXXXXXX",
        "      X X     X   X",
        "XXXXX X X XXX X X X",
        "X X   X   X X   X X",
        "X X XXX XXX XXXXX X",
        "X X X       X     X",
        "X X XXXXX XXX XXX X",
        "X X     X X   X   X",
        "X XXXXX XXX XXX XXX",
        "X     X     X X X X",
        "X XXXXXXXXXXX X X X",
        "X                  ",
        "XXXXXXXXXXXXXXXXXXX"
    };
 
    /* honnan indulunk, es hol a jutalomfalat */
    megfejt(p, 0, 1, MERETX - 1, MERETY - 2);
    kirajzol(p);
 
    return 0;
}
