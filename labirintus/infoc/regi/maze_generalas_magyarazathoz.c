#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <allegro.h>

#define MERETX 35
#define MERETY 25

/* egy cella merete */
#define PIXEL 16
/* kesleltetes */
#define SLEEP 50
/* fal szine */
#define FAL 15
/* jarat szine */
#define JARAT 0
/* terem szine ideiglenesen */
#define IDEIGLENES 1

typedef char Palya[MERETY][MERETX];

void save() {
    static int num = 0;
    static PALETTE pal;
    char filename[100];

    if (num == 0)
        get_palette(pal);

    sprintf(filename, "kep%08d.bmp", num++);
    save_bmp(filename, screen, pal);

}


void rajzol(Palya p, int x, int y, char mit) {
    p[y][x] = mit;
    rectfill(screen, x * PIXEL, y * PIXEL, x * PIXEL + PIXEL - 1, y * PIXEL + PIXEL - 1, mit);
}

/* az egesz palyat fallal tolti ki */
void ures(Palya p) {
    int x, y;

    for (y = 0; y < MERETY; ++y)
        for (x = 0; x < MERETX; ++x)
            rajzol(p, x, y, FAL);
}

void labirintus(Palya p, int x, int y) {
    typedef enum { fel, le, jobbra, balra } Irany;
    Irany iranyok[4] = {fel, le, jobbra, balra};
    int i;

    /* erre a pontra hivtak minket, ide lerakunk egy darabka jaratot. */
    rajzol(p, x, y, IDEIGLENES);
    /* kicsit varunk, hogy viccesen nezzen ki */
    rest(SLEEP);
    save();
    rest(SLEEP);
    save();

    /* a tomb keverese */
    for (i = 0; i < 4; ++i) {       /* mindegyiket... */
        int r = rand() % 4;         /* egy veletlenszeruen valasztottal... */
        Irany temp = iranyok[i];    /* megcsereljuk. */
        iranyok[i] = iranyok[r];
        iranyok[r] = temp;
    }

    /* a kevert iranyok szerint mindenfele probalunk menni, ha lehet. */
    for (i = 0; i < 4; ++i)
        switch (iranyok[i]) {
        case fel:
            if (y >= 2 && p[y - 2][x] == FAL) {
                rajzol(p, x, y - 1, IDEIGLENES); /* jel: "ezt a reszt meg epp rajzoljuk" */
                labirintus(p, x, y - 2); /* es rekurzive megyunk tovabb */
                rajzol(p, x, y - 1, JARAT); /* kesz vagyunk az innen kiindulo labirintussal */
            }
            break;
        case balra:
            if (x >= 2 && p[y][x - 2] == FAL) {
                rajzol(p, x - 1, y, IDEIGLENES);
                labirintus(p, x - 2, y);
                rajzol(p, x - 1, y, JARAT);
            }
            break;
        case le:
            if (y < MERETY - 2 && p[y + 2][x] == FAL) {
                rajzol(p, x, y + 1, IDEIGLENES);
                labirintus(p, x, y + 2);
                rajzol(p, x, y + 1, JARAT);
            }
            break;
        case jobbra:
            if (x < MERETX - 2 && p[y][x + 2] == FAL) {
                rajzol(p, x + 1, y, IDEIGLENES);
                labirintus(p, x + 2, y);
                rajzol(p, x + 1, y, JARAT);
            }
            break;
        }
    rajzol(p, x, y, JARAT);
    rest(SLEEP);
    save();
}

int main(void) {
    Palya p;
    int i;

    allegro_init();
    install_timer();
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, MERETX * PIXEL, MERETY * PIXEL, 0, 0);    /* ekkora ablakot kerunk */
    srand(time(0));

    /* generalas */
    ures(p);
    labirintus(p, 1, 1);
    rajzol(p, 0, 1, JARAT);
    rajzol(p, MERETX - 1, MERETY - 2, JARAT);

    rest(1000);
    for (i = 0; i < 10; ++i)
        save();

    return 0;
}
END_OF_MAIN()
