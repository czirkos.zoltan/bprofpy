#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <allegro.h>

#define MERETX 35
#define MERETY 25

/* egy cella merete */
#define PIXEL 16
/* kesleltetes */
#define SLEEP 50
/* fal szine */
#define FAL 15
/* jarat szine */
#define JARAT 0
/* kijarat fele vezeto ut szine */
#define KIJARAT 2
/* zsakutca szine */
#define ZSAKUTCA 4
/* kerdeses hely szine */
#define KERDESES 1

typedef char Palya[MERETY][MERETX];

void save() {
    static int num = 0;
    static PALETTE pal;
    char filename[100];

    if (num == 0)
        get_palette(pal);

    sprintf(filename, "kep%08d.bmp", num++);
    save_bmp(filename, screen, pal);

}

void rajzol(Palya p, int x, int y, char mit) {
    p[y][x] = mit;
    rectfill(screen, x * PIXEL, y * PIXEL, x * PIXEL + PIXEL - 1, y * PIXEL + PIXEL - 1, mit);
}

/* az egesz palyat fallal tolti ki */
void ures(Palya p) {
    int x, y;

    for (y = 0; y < MERETY; ++y)
        for (x = 0; x < MERETX; ++x)
            rajzol(p, x, y, FAL);
}

void labirintus(Palya p, int x, int y) {
    typedef enum { fel, le, jobbra, balra } Irany;
    Irany iranyok[4] = {fel, le, jobbra, balra};
    int i;

    /* erre a pontra hivtak minket, ide lerakunk egy darabka jaratot. */
    rajzol(p, x, y, JARAT);

    /* a tomb keverese */
    for (i = 0; i < 4; ++i) {       /* mindegyiket... */
        int r = rand() % 4;         /* egy veletlenszeruen valasztottal... */
        Irany temp = iranyok[i];    /* megcsereljuk. */
        iranyok[i] = iranyok[r];
        iranyok[r] = temp;
    }

    /* a kevert iranyok szerint mindenfele probalunk menni, ha lehet. */
    for (i = 0; i < 4; ++i)
        switch (iranyok[i]) {
        case fel:
            if (y >= 2 && p[y - 2][x] != JARAT) {
                rajzol(p, x, y - 1, JARAT); /* elinditjuk arrafele a jaratot */
                labirintus(p, x, y - 2); /* es rekurzive megyunk tovabb */
            }
            break;
        case balra:
            if (x >= 2 && p[y][x - 2] != JARAT) {
                rajzol(p, x - 1, y, JARAT);
                labirintus(p, x - 2, y);
            }
            break;
        case le:
            if (y < MERETY - 2 && p[y + 2][x] != JARAT) {
                rajzol(p, x, y + 1, JARAT);
                labirintus(p, x, y + 2);
            }
            break;
        case jobbra:
            if (x < MERETX - 2 && p[y][x + 2] != JARAT) {
                rajzol(p, x + 1, y, JARAT);
                labirintus(p, x + 2, y);
            }
            break;
        }
}

/* igazzal ter vissza, ha arrafele megtalalta,
 * hamissal, ha nem */
int megfejt(Palya p, int x, int y, int celx, int cely) {
    int megtalalt;

    rajzol(p, x, y, KERDESES);
    rest(SLEEP);
//    save();

    /* egyelore nem talaljuk a kijaratot */
    megtalalt = 0;

    /* pont a celnal allunk? */
    if (x == celx && y == cely)
        megtalalt = 1;

    /* ha meg nem talaltuk meg a kijaratot... ES ha tudunk jobbra menni... */
    if (!0 && x < MERETX - 1 && p[y][x + 1] == JARAT) {
        /* ha arra van a megfejtes */
        if(megfejt(p, x + 1, y, celx, cely))
            megtalalt = 1;
    }
    /* balra */
    if (!0 && x > 0 && p[y][x - 1] == JARAT) {
        if (megfejt(p, x - 1, y, celx, cely))
            megtalalt = 1;
    }
    if (!0 && y > 0 && p[y - 1][x] == JARAT) {
        if (megfejt(p, x, y - 1, celx, cely))
            megtalalt = 1;
    }
    if (!0 && y < MERETY - 1 && p[y + 1][x] == JARAT) {
        if (megfejt(p, x, y + 1, celx, cely))
            megtalalt = 1;
    }

    /* a hivottakbol kiderult, hogy ez a kijarathoz
     * vezeto ut, vagy nem. */
    rajzol(p, x, y, megtalalt ? KIJARAT : ZSAKUTCA);
    rest(SLEEP);
//    save();

    /* jelezzuk a hivonak, hogy valahonnan errefele indulva
     * lehet-e kijaratot talalni */
    return megtalalt;
}

int main(void) {
    Palya p;
    int i;

    allegro_init();
    install_timer();
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, MERETX * PIXEL, MERETY * PIXEL, 0, 0);    /* ekkora ablakot kerunk */
    srand(time(0));

    /* generalas */
    ures(p);
    labirintus(p, 1, 1);
    rajzol(p, 0, 1, JARAT);
    rajzol(p, MERETX - 1, MERETY - 2, JARAT);

    rest(1000);
    for (i = 0; i < 10; ++i)
        save();

    /* megfejtes */
    megfejt(p, 0, 1, MERETX - 1, MERETY - 2);

    rest(5000);
    for (i = 0; i < 10; ++i)
        save();

    return 0;
}
END_OF_MAIN()
