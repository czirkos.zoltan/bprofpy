#BProfPy pygame

Az infoC SDL-es anyagainak pygame-es átdolgozása.

## Struktúra
```
pygame
├── README.md
├── c                                       # Példaprogramok C-ben
│   └── ...
├── doc                                     
│   ├── img
│   │   └── ...
│   ├── pygame.md                           # Legfontosabb file: ebben van az írás :D
│   └── res
│       └── ...
└── python                                  # Példaprogramok pythonos átdolgozása
    └── ...
```

## TODO

Menő lenne, ha a script fileokból automatikusan átkerülnének a módosítások az írásba, de túl sok effortnak éreztem megoldani.

