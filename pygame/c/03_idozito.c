#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
 
 
/* kulon fuggvenybe, hogy olvashatobb legyen */
void sdl_init(int szeles, int magas, SDL_Window **pwindow, SDL_Renderer **prenderer) {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        SDL_Log("Nem indithato az SDL: %s", SDL_GetError());
        exit(1);
    }
    SDL_Window *window = SDL_CreateWindow("SDL peldaprogram", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, szeles, magas, 0);
    if (window == NULL) {
        SDL_Log("Nem hozhato letre az ablak: %s", SDL_GetError());
        exit(1);
    }
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
    if (renderer == NULL) {
        SDL_Log("Nem hozhato letre a megjelenito: %s", SDL_GetError());
        exit(1);
    }
    SDL_RenderClear(renderer);
 
    *pwindow = window;
    *prenderer = renderer;
}
 
 
/* ez a fuggveny hivodik meg az idozito altal.
 * betesz a feldolgozando esemenyek koze (push) egy felhasznaloi esemenyt */
Uint32 idozit(Uint32 ms, void *param) {
    SDL_Event ev;
    ev.type = SDL_USEREVENT;
    SDL_PushEvent(&ev);
    return ms;   /* ujabb varakozas */
}
 
 
int main(int argc, char *argv[]) {
    enum { ABLAK=360 };
    enum { GOLYO_R=10 };
    typedef struct Golyo {
        int x, y;
        int vx, vy;
    } Golyo;
    
    
    SDL_Window *window;
    SDL_Renderer *renderer;
    sdl_init(ABLAK, ABLAK, &window, &renderer);
 
    /* idozito hozzaadasa: 20 ms; 1000 ms / 20 ms -> 50 fps */
    SDL_TimerID id = SDL_AddTimer(20, idozit, NULL);
 
    /* animaciohoz */
    Golyo g;
    g.x = ABLAK/2;
    g.y = ABLAK/3;
    g.vx = 3;
    g.vy = 2;
 
    /* szokasos esemenyhurok */
    bool quit = false;
    while (!quit) {
        SDL_Event event;
        SDL_WaitEvent(&event);
 
        switch (event.type) {
            /* felhasznaloi esemeny: ilyeneket general az idozito fuggveny */
            case SDL_USEREVENT:
                /* kitoroljuk az elozo poziciojabol (nagyjabol) */
                filledCircleRGBA(renderer, g.x, g.y, GOLYO_R, 0x20, 0x20, 0x40, 0xFF);
                /* kiszamitjuk az uj helyet */
                g.x += g.vx;
                g.y += g.vy;
                /* visszapattanás */
                if (g.x < GOLYO_R || g.x > ABLAK-GOLYO_R)
                    g.vx *= -1;
                if (g.y < GOLYO_R || g.y > ABLAK-GOLYO_R)
                    g.vy *= -1;
                /* ujra kirajzolas, es mehet a kepernyore */
                filledCircleRGBA(renderer, g.x, g.y, GOLYO_R, 0x80, 0x80, 0xFF, 0xFF);
                SDL_RenderPresent(renderer);
                break;
 
            case SDL_QUIT:
                quit = true;
                break;
        }
    }
    /* idozito torlese */
    SDL_RemoveTimer(id);
 
    SDL_Quit();
 
    return 0;
}

