#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL2_gfxPrimitives.h>
 
 
void sdl_init(int szeles, int magas, SDL_Window **pwindow, SDL_Renderer **prenderer) {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        SDL_Log("Nem indithato az SDL: %s", SDL_GetError());
        exit(1);
    }
    SDL_Window *window = SDL_CreateWindow("SDL peldaprogram", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, szeles, magas, 0);
    if (window == NULL) {
        SDL_Log("Nem hozhato letre az ablak: %s", SDL_GetError());
        exit(1);
    }
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
    if (renderer == NULL) {
        SDL_Log("Nem hozhato letre a megjelenito: %s", SDL_GetError());
        exit(1);
    }
 
    *pwindow = window;
    *prenderer = renderer;
}
 
 
int main(int argc, char *argv[]) {
    SDL_Window *window;
    SDL_Renderer *renderer;
    sdl_init(480, 200, &window, &renderer);
 
    SDL_Color feher = {255, 255, 255}, piros = {255, 0, 0};
 
    /* h[M(l@atter */
    int i;
    for (i = 0; i < 500; ++i)
        filledCircleRGBA(renderer, rand() % 480, rand() % 200,
                         10 + rand() % 5, rand() % 256, rand() % 256, rand() % 256, 64);
 
    /* betutipus betoltese, 32 pont magassaggal */
    TTF_Init();
    TTF_Font *font = TTF_OpenFont("LiberationSerif-Regular.ttf", 32);
    if (!font) {
        SDL_Log("Nem sikerult megnyitni a fontot! %s\n", TTF_GetError());
        exit(1);
    }
 
    /* felirat megrajzolasa, kulonfele verziokban */
    SDL_Surface *felirat;
    SDL_Texture *felirat_t;
    SDL_Rect hova = { 0, 0, 0, 0 };
 
    /* ha sajat kodban hasznalod, csinalj belole fuggvenyt! */
    felirat = TTF_RenderUTF8_Solid(font, "TTF_RenderUTF8_Solid()", feher);
    felirat_t = SDL_CreateTextureFromSurface(renderer, felirat);
    hova.x = (480 - felirat->w) / 2;
    hova.y = 20;
    hova.w = felirat->w;
    hova.h = felirat->h;
    SDL_RenderCopy(renderer, felirat_t, NULL, &hova);
    SDL_FreeSurface(felirat);
    SDL_DestroyTexture(felirat_t);
 
    /* ha sajat kodban hasznalod, csinalj belole fuggvenyt! */
    felirat = TTF_RenderUTF8_Shaded(font, "TTF_RenderUTF8_Shaded()", feher, piros);
    felirat_t = SDL_CreateTextureFromSurface(renderer, felirat);
    hova.x = (480 - felirat->w) / 2;
    hova.y = 60;
    hova.w = felirat->w;
    hova.h = felirat->h;
    SDL_RenderCopy(renderer, felirat_t, NULL, &hova);
    SDL_FreeSurface(felirat);
    SDL_DestroyTexture(felirat_t);
 
    /* ha sajat kodban hasznalod, csinalj belole fuggvenyt! */
    felirat = TTF_RenderUTF8_Blended(font, "TTF_RenderUTF8_Blended()", feher);
    felirat_t = SDL_CreateTextureFromSurface(renderer, felirat);
    hova.x = (480 - felirat->w) / 2;
    hova.y = 100;
    hova.w = felirat->w;
    hova.h = felirat->h;
    SDL_RenderCopy(renderer, felirat_t, NULL, &hova);
    SDL_FreeSurface(felirat);
    SDL_DestroyTexture(felirat_t);
 
    /* tudja az ekezeteket */
    /* ez az utf8 szoveg azert nez ki ilyen rosszul, mert szinte csak ekezetes betu van benne.
     * amugy a code::bl[M+l@ocks-ban utf8 forrasfajlt kell beallitani, es akkor irhato kozvetlenul
     * ekezetes betu ide. */
    felirat = TTF_RenderUTF8_Blended(font, "\xC3\xA1rv\xC3\xADzt\xC5\xB1r\xC5\x91 "
                                           "t\xC3\xBCk\xC3\xB6rf\xC3\xBAr\xC3\xB3g\xC3\xA9p "
                                           "\xE2\x98\xBA \xE2\x82\xAC", feher);
    felirat_t = SDL_CreateTextureFromSurface(renderer, felirat);
    hova.x = (480 - felirat->w) / 2;
    hova.y = 140;
    hova.w = felirat->w;
    hova.h = felirat->h;
    SDL_RenderCopy(renderer, felirat_t, NULL, &hova);
    SDL_FreeSurface(felirat);
    SDL_DestroyTexture(felirat_t);
 
    SDL_RenderPresent(renderer);
 
    /* nem kell tobbe */
    TTF_CloseFont(font);
 
    SDL_Event event;
    while (SDL_WaitEvent(&event) && event.type != SDL_QUIT) {
    }
 
    SDL_Quit();
 
    return 0;
}

