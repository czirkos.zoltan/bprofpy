import random

import pygame
import pygame.gfxdraw


def main():
    # pygame inicializálás
    pygame.init()

    # ablak megnyitás
    window = pygame.display.set_mode((480, 200))
    pygame.display.set_caption('pygame példaprogram')

    white = pygame.Color('#FFFFFF')
    red = pygame.Color('#FF0000')

    # háttér véletlenszerű elhelyezkedésű és színű körökkel
    for i in range(500):
        pygame.gfxdraw.filled_circle(window, random.randrange(0, 480), random.randrange(0, 200),
                                     10 + random.randrange(0, 5), pygame.Color(random.randrange(0, 256),
                                                                               random.randrange(0, 256),
                                                                               random.randrange(0, 256),
                                                                               64))

    # betűtípus betöltése, 32 pont magasságal
    font = pygame.font.Font('LiberationSerif-Regular.ttf', 32)

    # különféle kiírások
    text = font.render('No AntiAlias', False, white)
    window.blit(text, ((480 - text.get_width()) / 2, 20))

    text = font.render('With Background', False, white, red)
    window.blit(text, ((480 - text.get_width()) / 2, 60))

    text = font.render('Yes AntiAlias', True, white)
    window.blit(text, ((480 - text.get_width()) / 2, 100))

    text = font.render('Még ékezettel is működik', True, white, red)
    window.blit(text, ((480 - text.get_width()) / 2, 140))

    # ki a képernyőre
    pygame.display.update()

    # szokásos várakozás a kilépésre
    quit = False
    while not quit:
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            quit = True

    pygame.quit()


main()
