import pygame
import pygame.gfxdraw


def main():
    # pygame inicializálás
    pygame.init()

    # ablak megnyitás
    window = pygame.display.set_mode((350, 200))
    pygame.display.set_caption('pygame példaprogram')

    quit = False
    left = False
    right = False
    draw = True

    while not quit:
        if draw:
            if left:
                pygame.gfxdraw.filled_trigon(window, 50, 100, 150, 50, 150, 150, pygame.Color("#00C000"))
            else:
                pygame.gfxdraw.filled_trigon(window, 50, 100, 150, 50, 150, 150, pygame.Color("#FF0000"))
            if right:
                pygame.gfxdraw.filled_trigon(window, 300, 100, 200, 50, 200, 150, pygame.Color("#00C000"))
            else:
                pygame.gfxdraw.filled_trigon(window, 300, 100, 200, 50, 200, 150, pygame.Color("#FF0000"))
            pygame.display.update()
            draw = False

        event = pygame.event.wait()
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                left = False
                draw = True
            if event.key == pygame.K_RIGHT:
                right = False
                draw = True
            if event.key == pygame.K_ESCAPE:
                quit = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                left = True
                draw = True
            if event.key == pygame.K_RIGHT:
                right = True
                draw = True

        if event.type == pygame.QUIT:
            quit = True

    pygame.quit()


main()
