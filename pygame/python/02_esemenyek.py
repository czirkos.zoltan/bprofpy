import pygame
import pygame.gfxdraw


def main():
    # pygame inicializálás
    pygame.init()

    # ablak megnyitás
    window = pygame.display.set_mode((440, 360))
    pygame.display.set_caption('pygame példaprogram')

    # kirajzolás, hogy ne üres ablakkal kezdjünk
    pygame.display.update()

    # az eseményvezérelt hurok
    quit = False
    click = False
    prev_x = 0
    prev_y = 0
    while not quit:
        drawn = False

        event = pygame.event.wait()
        # egér kattintás
        if event.type == pygame.MOUSEBUTTONDOWN:
            # bal gomb = 1, középső gomb = 2, jobb gomb = 3
            if event.button == 1:
                click = True
                prev_x, prev_y = event.pos
            elif event.button == 3:
                window.fill(pygame.Color('#000000'))
                drawn = True

        # egérgomb elengedése
        if event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                click = False

        # egér mozgás
        if event.type == pygame.MOUSEMOTION:
            x, y = event.pos
            if click:
                pygame.gfxdraw.line(window, prev_x, prev_y, x, y, pygame.Color('#FFFFFF'))
                drawn = True
            # a következő mozdulat eseményhez
            prev_x, prev_y = x, y

        # ablak bezárása
        if event.type == pygame.QUIT:
            quit = True

        if drawn:
            pygame.display.update()

    pygame.quit()


main()
